#!/usr/bin/python
import sys
import json
from game_of_life import GameOfLife, GameOfLifeFileParser


def main():
    file = sys.argv[1]

    num_iterations = long(sys.argv[2])

    starting_grid = GameOfLifeFileParser.parse_file(file)

    game_of_life = GameOfLife(starting_grid)

    print game_of_life.get_pretty_format_current_generation()
    for x in range(0, num_iterations):
        game_of_life.move_to_and_get_next_generation()
        print game_of_life.get_pretty_format_current_generation()


if __name__ == "__main__":
    main()

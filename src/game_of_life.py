class GameOfLifeFileParser:
    @staticmethod
    def parse_file(file):
        with open(file, 'r') as myfile:
            starting_grid = list()
            for line in myfile:
                row = list()
                for char in line:
                    if char != '\n':
                        row.append(char)

                starting_grid.append(row)

            myfile.close()

        return starting_grid


class CellState:
    DEAD = '.'
    ALIVE = '*'


class GameOfLife:
    def __init__(self, starting_grid):
        self._grid_state = starting_grid

        self._current_generation = 1

        self._grid_height = len(starting_grid)

        if self._grid_height <= 0:
            raise Exception("The input file's starting grid must have a height greater than 0")

        self._grid_width = len(starting_grid[0])

        if self._grid_width <= 0:
            raise Exception("The input file's starting grid must have a width greater than 0")

    def move_to_and_get_next_generation(self):
        #  Initialize an empty next grid state
        next_grid_state = list()

        # For each row...
        for row_index, row in enumerate(self._grid_state):
            new_row = list()
            # For each cell...
            for column_index, cell_state in enumerate(row):
                # Determine next cell state and append result to the new grid state list
                num_live_neighbors = self._get_cells_live_neighbors(row_index, column_index)
                next_cell_state = self._get_cell_state_for_next_generation(cell_state, num_live_neighbors)
                new_row.append(next_cell_state)

            next_grid_state.append(new_row)

        self._grid_state = next_grid_state
        self._current_generation += 1

        return self._grid_state

    def get_pretty_format_current_generation(self):
        pretty_string = 'Generation ' + str(self._current_generation) + '\n'

        for row in self._grid_state:
            for cell_state in row:
                if cell_state == CellState.DEAD:
                    pretty_string += '.'
                else:
                    pretty_string += '*'

                pretty_string += ' '

            pretty_string += '\n'

        return pretty_string

    def _get_cell_state_for_next_generation(self, current_cell_state, neighbor_count):

        if current_cell_state == CellState.ALIVE:  # Die from loneliness
            if neighbor_count < 2:
                return CellState.DEAD

            if neighbor_count > 3:  # Die from over population
                return CellState.DEAD

        else:  # Dead Cell State
            if neighbor_count == 3:  # Come to life
                return CellState.ALIVE

        # If none of the conditions are met then the state stays the same
        return current_cell_state

    def _get_cells_live_neighbors(self, row_index, column_index):
        neighbor_count = 0

        # I am sure there is a more elegant algorithm than this
        # but we are just going to check each neighbor individually

        # Check cells to the left
        if column_index >= 1:
            if self._grid_state[row_index][column_index - 1] == CellState.ALIVE:
                neighbor_count += 1

            # Check cell to top left
            if row_index >= 1:
                if self._grid_state[row_index - 1][column_index - 1] == CellState.ALIVE:
                    neighbor_count += 1

            # Check cell to bottom left
            if row_index < self._grid_height-1:
                if self._grid_state[row_index + 1][column_index - 1] == CellState.ALIVE:
                    neighbor_count += 1

        # Check cells to the right
        if column_index < self._grid_width-1:
            if self._grid_state[row_index][column_index + 1] == CellState.ALIVE:
                neighbor_count += 1

            # Check cell to top right
            if row_index >= 1:
                if self._grid_state[row_index - 1][column_index + 1] == CellState.ALIVE:
                    neighbor_count += 1

            # Check cell to bottom right
            if row_index < self._grid_height-1:
                if self._grid_state[row_index + 1][column_index + 1] == CellState.ALIVE:
                    neighbor_count += 1

        # Check cell to bottom
        if row_index < self._grid_height-1:
            if self._grid_state[row_index + 1][column_index] == CellState.ALIVE:
                neighbor_count += 1

        # Check cell to top
        if row_index >= 1:
            if self._grid_state[row_index - 1][column_index] == CellState.ALIVE:
                neighbor_count += 1

        return neighbor_count
import unittest
import sys
import os.path

# Import application code here ...
from src.game_of_life import GameOfLife, CellState

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


class TestGameOfLife(unittest.TestCase):

    def setUp(self):
        pass

    def test_live_cell_with_fewer_than_two_live_neighbors_dies(self):
        # 0 live neighbors
        game_of_life = GameOfLife([
            [CellState.DEAD, CellState.DEAD, CellState.DEAD],
            [CellState.ALIVE, CellState.DEAD, CellState.DEAD],
            [CellState.DEAD, CellState.DEAD, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.DEAD

        # 1 live neighbor
        game_of_life = GameOfLife([
            [CellState.DEAD, CellState.DEAD, CellState.DEAD],
            [CellState.ALIVE, CellState.DEAD, CellState.DEAD],
            [CellState.ALIVE, CellState.DEAD, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.DEAD

    def test_live_cell_with_more_than_three_live_neighbors_dies(self):
        # 8 live neighbors
        game_of_life = GameOfLife([
            [CellState.ALIVE, CellState.ALIVE, CellState.ALIVE],
            [CellState.ALIVE, CellState.ALIVE, CellState.ALIVE],
            [CellState.ALIVE, CellState.ALIVE, CellState.ALIVE]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][1] == CellState.DEAD

        # 7 live neighbors
        game_of_life = GameOfLife([
            [CellState.DEAD, CellState.ALIVE, CellState.ALIVE],
            [CellState.ALIVE, CellState.ALIVE, CellState.ALIVE],
            [CellState.ALIVE, CellState.ALIVE, CellState.ALIVE]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][1] == CellState.DEAD

        # 6 live neighbors
        game_of_life = GameOfLife([
            [CellState.ALIVE, CellState.ALIVE, CellState.ALIVE],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][1] == CellState.DEAD

        # 5 live neighbors
        game_of_life = GameOfLife([
            [CellState.DEAD, CellState.ALIVE, CellState.ALIVE],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][1] == CellState.DEAD

        # 4 live neighbors
        game_of_life = GameOfLife([
            [CellState.ALIVE, CellState.DEAD, CellState.ALIVE],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD],
            [CellState.DEAD, CellState.ALIVE, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][1] == CellState.DEAD

    def test_live_cell_with_two_or_three_live_neighbors_lives(self):
        game_of_life = GameOfLife([
            [CellState.DEAD, CellState.DEAD, CellState.DEAD],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD],
            [CellState.DEAD, CellState.ALIVE, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.ALIVE

        game_of_life = GameOfLife([
            [CellState.DEAD, CellState.DEAD, CellState.DEAD],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD],
            [CellState.ALIVE, CellState.ALIVE, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][0] == CellState.ALIVE

    def test_dead_cell_with_exactly_three_live_neighbors_becomes_live(self):
        game_of_life = GameOfLife([
            [CellState.DEAD, CellState.ALIVE, CellState.DEAD],
            [CellState.ALIVE, CellState.DEAD, CellState.DEAD],
            [CellState.DEAD, CellState.ALIVE, CellState.DEAD]
        ])

        new_generation = game_of_life.move_to_and_get_next_generation()

        assert new_generation[1][1] == CellState.ALIVE


if __name__ == '__main__':
    unittest.main()

Game Of Life Kata

To run make sure python is installed

# Game Of Life Kata

The Game of Life, is a cellular automation simulation invented by the British mathematician
John Horton Conway in 1970. For more background information, see https://en.wikipedia.org/
wiki/Conway%27s_Game_of_Life

Goal

In this kata you will write a program that calculates the next generation of Conway’s game of
life, given an initial state.

Rules of the Game

The board (or playing field) for the Game of Life is a two dimensional grid of cells. Each cell is
considered to be either “alive” or “dead”. The next generation of the grid is calculated using
these rules:
1. Any live cell with fewer than two live neighbors dies, as if caused by under
population.
2. Any live cell with more than three live neighbors dies, as if by overcrowding.
3. Any live cell with two or three live neighbors lives on to the next generation.
4. Any dead cell with exactly three live neighbors becomes a live cell.
5. A cell’s neighbors are those cells which are horizontally, vertically or
diagonally adjacent. Most cells will have eight neighbors. Cells placed on the
edge of the grid will have fewer.

## Getting Started

Clone or download a zip this repository

### Prerequisites

Make sure you have python installed. That should be all you need.

### Running

The program accepts two arguments:
1. A file pointing to the initial generation in a format where '.' represents a dead cell and '*' represents a live one
2. Number of iterations to run - must be an integer

The following command would run the 15 iterations using the starting generation in input_file_2.txt
Example: Running from the root directory:
```
python src/audition.py example_input_files/input_file_3.txt 15
```

## Author

* **Dillon Courts** - *Initial work* - [DillonCourts](https://bitbucket.org/dillon_courts/)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
